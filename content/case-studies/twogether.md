---
title: Kubernetes and GitLab Auto DevOps
---

A client came to us with an existing containerised application which was
current being deployed in production via a docker swarm and a handful of
custom written bash scripts. This was causing them some stability
issues, not to mention the knowledge of how most of the scripts worked
had been lost.

We were asked to transition the stack onto a new Kubernetes cluster,
with a focus on taking the simpliest, "least amount of code" options for
each design decision. The client already ran their own GitLab instance,
so we leant heavily on GitLab Auto DevOps and GitLab CI/CD.

The cluster itself was hosted in AWS EKS, with the entirity of the VPC
and cluster setup written in Terraform, including deploying all required
cluster wide helm projects (cert-manager, ingress-nginx, and a GitLab
kubernetes runner).

